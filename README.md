# Hello Tide

Example that demonstrates how you can build and run software in containers for multiple architectures.

This ultra-simple code example uses Rust and the Tide framework, hence the name.

Below are instructions for building for x86_64, ppc64le and aarch64 architectures using multiarch/alpine images setup with qemu static binaries for cross compilation.

Replace ```mnellemann/hellotide``` with your own container registry username and repository.

```shell
export DOCKER_BUILDKIT=1

# Build and push x86_64 target
docker build --build-arg TARGET=x86_64 -t mnellemann/hellotide:x86_64-linux-latest .
docker push mnellemann/hellotide:x86_64-linux-latest

# Build and push ppc64le target
docker build --platform ppc64le --build-arg TARGET=ppc64le -t mnellemann/hellotide:ppc64le-linux-latest .
docker push mnellemann/hellotide:ppc64le-linux-latest

# Build and push aarch64 target
docker build --platform aarch64 --build-arg TARGET=aarch64 -t mnellemann/hellotide:aarch64-linux-latest .
docker push mnellemann/hellotide:aarch64-linux-latest

# Create and push a manifest (docker experimental feature)
# This enables you to pull mnellemann/hellotide:latest and get the right image depending on your platform
docker manifest create --amend \
  mnellemann/hellotide:latest \
  mnellemann/hellotide:x86_64-linux-latest \
  mnellemann/hellotide:ppc64le-linux-latest \
  mnellemann/hellotide:aarch64-linux-latest
docker manifest push mnellemann/hellotide:latest
```
