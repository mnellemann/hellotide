# TARGET can be x86, x86_64, ppc64le or aarch64
ARG TARGET=x86_64

# VERSION can be latest-stable, v3.13, v3.14, etc.
ARG VERSION=v3.14

# BASEIMAGE
ARG BASE_IMAGE=multiarch/alpine:${TARGET}-${VERSION}


###
### Stage 1 - Builder Container
###

FROM ${BASE_IMAGE} as builder

# Install build requirements
RUN apk update && apk add --no-cache cargo

# Copy our application sourcecode to the container
WORKDIR /tmp/build/
COPY . /tmp/build/

# Build our application from the sourcecode
RUN cargo build --release



###
### Stage 1 - Runtime Container
###

FROM ${BASE_IMAGE} as runtime

# Install runtime requirements
RUN apk update && apk add --no-cache libgcc 

# Copy our binary artifact from the previous building stage
COPY --from=builder /tmp/build/target/release/hellotide /opt/app/

# Instructions for running our application
USER nobody
WORKDIR /opt/app
ENTRYPOINT ["/opt/app/hellotide"]
